//@todo: Необходимо сделать так, чтобы склеивались css файлы, а так же css и js файлы из папок plugins. Исправить ассеты для комментариев.
//@todo: Задача: #279.
var gulp = require('gulp'),
    rigger = require('gulp-rigger'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    csso = require('gulp-csso'),
    rename = require('gulp-rename'),
    fs = require('fs'),
    yaml = require('js-yaml'),
    del = require('del'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect'),
    imagemin = require('gulp-imagemin'),
    svgmin = require('gulp-svgmin'),
	run = require('gulp-run'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    babel = require('gulp-babel'),
    plumber = require('gulp-plumber');

var config = loadConfig();

function loadConfig() {
    var ymlFile = fs.readFileSync('config.yml', 'utf8');
    return yaml.load(ymlFile);
}

gulp.task('connect', function () {    
    connect.server({
        root: 'build',
        livereload: true,
        port: 8282
    });
});

/**
 /* Tasks
 */

gulp.task('clean', function () {
    del.sync('build');
    del.sync('web');
});

gulp.task('miniSVG', function () {
    return gulp.src(config.PATH.src + '/img/**/*.svg')
        .pipe(svgmin())
        .pipe(gulp.dest(config.PATH.web + '/img'))
        .pipe(gulp.dest('build/assets/img'));
});

gulp.task('images', function () {
    return gulp.src(config.PATH.src + '/img/**/*.{png,jpg,gif}')
        .pipe(imagemin([
            imagemin.optipng({optimizationLevel: 3}),
            imagemin.jpegtran({progressive: true})
        ]))
        .pipe(gulp.dest(config.PATH.web + '/img'))
        .pipe(gulp.dest('build/assets/img'));
});

gulp.task('fonts', function () {
    return gulp.src(config.PATH.src + '/fonts/*')
        .pipe(gulp.dest(config.PATH.web + '/fonts'))
        .pipe(gulp.dest('build/assets/fonts'));
});

gulp.task('styles', function () {
    return gulp.src(config.PATH.src + '/sass/*.{sass,scss}')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(postcss([
            autoprefixer({
                browsers: [
                    'last 2 versions',
                    'Explorer 11',
                    'Edge >= 12'
                ],
                cascade: false
            })
        ]))
        .pipe(csso())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.PATH.web + '/css'))
        .pipe(gulp.dest('build/assets/css'))
        .pipe(connect.reload());
});

gulp.task('scripts', function () {
    return gulp.src(config.PATH.src + '/js/*.js')
        .pipe(concat('app.js'))
        .pipe(babel())
        .pipe(gulp.dest(config.PATH.web + '/js'))
        .pipe(gulp.dest('build/assets/js'))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(config.PATH.web + '/js'))
        .pipe(gulp.dest('build/assets/js'))
        .pipe(connect.reload());
});
gulp.task('vendor', function () {
    return gulp.src([config.PATH.src + '/js/libs/**/*.js', '!js/libs/bootstrap/*.js', config.PATH.src + '/js/libs/bootstrap/index.js', config.PATH.src + '/js/plugins/**/*.js'])
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.PATH.web + '/js'))
        .pipe(gulp.dest('build/assets/js'))
        .pipe(connect.reload());
});

gulp.task('json', function () {
    return gulp.src(config.PATH.src + '/js/*.json')
        .pipe(gulp.dest('build/assets/data'))
        .pipe(connect.reload());
});


gulp.task('views', function () {
    return gulp.src(config.PATH.src + '/views/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('build/'));
});

gulp.task('watch', function () {
    gulp.watch(config.PATH.src + '/js/*.js', ['scripts']);
    gulp.watch([config.PATH.src + '/js/libs/**/*.js', '!js/libs/bootstrap/*.js', config.PATH.src + '/js/libs/bootstrap/index.js', config.PATH.src + '/js/plugins/**/*.js'], ['vendor']);
    gulp.watch(config.PATH.src + '/sass/**/*.{sass,scss}', ['styles']);
    gulp.watch(config.PATH.src + '/js/**/*.json', ['json']);
    gulp.watch(config.PATH.src + '/views/**/*.html', ['views']);
});

gulp.task('build', ['views', 'styles', 'miniSVG', 'images', 'fonts', 'scripts', 'vendor', 'views']);
gulp.task('dev', ['build', 'watch', 'connect']);
gulp.task('prod'); // TODO
