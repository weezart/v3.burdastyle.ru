$(function () {
    const main_nav_menu = new Swiper('.main_navigation_wrapper .swiper-container', {
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.swiper-nav-next',
            prevEl: '.swiper-nav-prev',
        }  
    });
    const main_news_menu = new Swiper('.main_news_wrapper .swiper-container', {
        slidesPerView: 'auto'
    });
    const menuSticky = new hcSticky('.main_navigation_wrapper', {
        stickTo: '.layout',
        onStart: function() {
            $('.main_header').addClass('fixed');
            main_nav_menu.destroy(false, true);
            main_nav_menu.init();
        },
        onStop: function() {
            $('.main_header').removeClass('fixed');
        },
    });
    // Показ поискового поля, если оно было скрыто.
    $('.search_block button').click(function(e){
        e.preventDefault();
        if ($('.search_block input').val()) {
            console.log('Отправка данных из поискового поля.');
            $('.search_block form').submit();
        } else {
            $('.actions_set').toggleClass('full');
        }
    });
    // Тестируем брендинг, после удаляем этот код.
    $('.logo').click(function(e) {
        e.preventDefault();
        if ($('body').hasClass('branding')) {
            $('body').removeClass('branding').attr('style', '');
        } else {
            $('body').addClass('branding').css({
                'padding-top': '100px',
                'background-image': 'url(http://pattern4site.ru/images/opacity/_rebig/13-fon-dlya-sayta.png)'
            });
        }
    });
});